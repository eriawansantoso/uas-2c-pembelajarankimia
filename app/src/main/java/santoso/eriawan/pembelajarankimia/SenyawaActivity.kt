package santoso.eriawan.pembelajarankimia

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_senyawa.*
import org.json.JSONArray
import org.json.JSONObject

class SenyawaActivity  : AppCompatActivity(), View.OnClickListener{

    lateinit var senyawaAdapter : AdapterSenyawa
    var daftarSenyawa = mutableListOf<HashMap<String,String>>()
    var id_senyawa = ""
    val mainUrl = "http://192.168.43.104/uas-2c-kimiawebserver/"
    val url = mainUrl+"show_senyawa.php"
    val url2 = mainUrl+"query_senyawa.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsSe -> {
                query("insert")
            }
            R.id.btnUpSe -> {
                query("update")
            }
            R.id.btnDelSe -> {
                query("delete")
            }
            R.id.btnFindSe -> {
                showData(edNmSenyawa.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_senyawa)
        senyawaAdapter = AdapterSenyawa(daftarSenyawa,this)
        lsVideo.layoutManager = LinearLayoutManager(this)
        lsVideo.adapter = senyawaAdapter
        btnInsSe.setOnClickListener(this)
        btnUpSe.setOnClickListener(this)
        btnDelSe.setOnClickListener(this)
        btnFindSe.setOnClickListener(this)

        val actionBar = supportActionBar
        actionBar!!.title = "Kelola Data Senyawa"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)

    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun showData(namaSe : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarSenyawa.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sen = HashMap<String,String>()
                    sen.put("id_senyawa",jsonObject.getString("id_senyawa"))
                    sen.put("kation",jsonObject.getString("kation"))
                    sen.put("anion",jsonObject.getString("anion"))
                    sen.put("rumus_senyawa",jsonObject.getString("rumus_senyawa"))
                    sen.put("nama_senyawa",jsonObject.getString("nama_senyawa"))
                    daftarSenyawa.add(sen)
                }
                senyawaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_senyawa",namaSe)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun query(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showData("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_senyawa",id_senyawa)
                        hm.put("kation",edKation.text.toString())
                        hm.put("anion",edAnion.text.toString())
                        hm.put("rumus_senyawa",edRumus.text.toString())
                        hm.put("nama_senyawa",edNmSenyawa.text.toString())
                    }
                    "insert" -> {
                        hm.put("mode","insert")
                        hm.put("kation",edKation.text.toString())
                        hm.put("anion",edAnion.text.toString())
                        hm.put("rumus_senyawa",edRumus.text.toString())
                        hm.put("nama_senyawa",edNmSenyawa.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_senyawa",id_senyawa)
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    override fun onStart() {
        super.onStart()
        showData("")
    }

    fun clearInput(){
        id_senyawa = ""
        edKation.setText("")
        edAnion.setText("")
        edRumus.setText("")
        edNmSenyawa.setText("")
    }
}