package santoso.eriawan.pembelajarankimia

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_senyawa_list.*
import org.json.JSONArray

class SenyawaListActivity  : AppCompatActivity(), View.OnClickListener{

    lateinit var senyawaAdapter: AdapterSenyawaList
    var daftarSen = mutableListOf<HashMap<String, String>>()
    val mainurl = "http://192.168.43.104/uas-2c-kimiawebserver/"
    val url = mainurl+"show_senyawa.php"

    var imStr = ""
    var pilihGolongan = ""
    var id_senyawa = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_senyawa_list)
        senyawaAdapter = AdapterSenyawaList(daftarSen, this)
        lsListSen.layoutManager = LinearLayoutManager(this)
        lsListSen.adapter = senyawaAdapter

        btFindSe.setOnClickListener(this)
        val actionBar = supportActionBar
        actionBar!!.title = "Senyawa Kimia"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btFindSe -> {
                show(edCariSen.text.toString().trim())
            }
        }
    }
    fun show(namaSe : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarSen.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sen = HashMap<String,String>()
                    sen.put("id_senyawa",jsonObject.getString("id_senyawa"))
                    sen.put("kation",jsonObject.getString("kation"))
                    sen.put("anion",jsonObject.getString("anion"))
                    sen.put("rumus_senyawa",jsonObject.getString("rumus_senyawa"))
                    sen.put("nama_senyawa",jsonObject.getString("nama_senyawa"))
                    daftarSen.add(sen)
                }
                senyawaAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_senyawa",namaSe)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    override fun onStart() {
        super.onStart()
        show("")
    }
}