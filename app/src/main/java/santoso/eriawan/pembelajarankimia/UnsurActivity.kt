package santoso.eriawan.pembelajarankimia

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_unsur.*
import kotlinx.android.synthetic.main.row_unsur.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class UnsurActivity:AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper: MediaHelper
    lateinit var UnsurAdapter: AdapterUnsur
    lateinit var GolAdapter: ArrayAdapter<String>
    var daftarUnsur = mutableListOf<HashMap<String, String>>()
    var daftarGolongan = mutableListOf<String>()
    val mainurl = "http://192.168.43.104/uas-2c-kimiawebserver/"
    val url = mainurl+"show_unsur.php"
    val url2 = mainurl+"show_golongan.php"
    val url3 = mainurl+"query_unsur.php"
    var imStr = ""
    var pilihGolongan = ""
    var iduns = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_unsur)
        UnsurAdapter = AdapterUnsur(daftarUnsur, this)
        mediaHelper = MediaHelper(this)
        lsUnsur.layoutManager = LinearLayoutManager(this)
        lsUnsur.adapter = UnsurAdapter

        GolAdapter = ArrayAdapter(
            this, android.R.layout.simple_dropdown_item_1line,
            daftarGolongan
        )
        spinner2.adapter = GolAdapter
        spinner2.onItemSelectedListener = itemSelected

        imageView2.setOnClickListener(this)
        btUpUnsur.setOnClickListener(this)
        btDelUnsur.setOnClickListener(this)
        btInUnsur.setOnClickListener(this)
        btnFind.setOnClickListener(this)

        val actionBar = supportActionBar
        actionBar!!.title = "Kelola Data Unsur"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imageView2 -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent, mediaHelper.getRcGallery())
            }
            R.id.btInUnsur -> {
                query_unsur("insert")
            }
            R.id.btDelUnsur -> {
                query_unsur("delete")
            }
            R.id.btUpUnsur -> {
                query_unsur("update")
            }
            R.id.btnFind -> {
                showDataUnsur(edNama1.text.toString().trim())
            }
        }
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener {
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner2.setSelection(0)
            pilihGolongan = daftarGolongan.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihGolongan = daftarGolongan.get(position)
        }
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr = mediaHelper.getBitmapToString(data!!.data,imageView2)
            }
        }
    }

    fun query_unsur(mode : String){
        val request = object : StringRequest(
            Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showDataUnsur("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                   "insert" -> {
                       hm.put("mode","insert")
                       hm.put("no_atom",edNoAt1.text.toString())
                       hm.put("simbol",edSim1.text.toString())
                       hm.put("nama_unsur", edNama1.text.toString())
                       hm.put("massa_atom", edMassa1.text.toString())
                       hm.put("electron",edEle1.text.toString())
                       hm.put("proton" , edProt1.text.toString())
                       hm.put("neutron" , edNeu1.text.toString())
                       hm.put("nama_gol",pilihGolongan)
                       hm.put("keterangan" , edKet1.text.toString())
                       hm.put("image" , imStr)
                       hm.put("file", nmFile)
                   }
                    "update" -> {
                        hm.put("mode", "update")
                        hm.put("no_atom", edNoAt1.text.toString())
                        hm.put("simbol", edSim1.text.toString())
                        hm.put("nama_unsur", edNama1.text.toString())
                        hm.put("massa_atom", edMassa1.text.toString())
                        hm.put("electron", edEle1.text.toString())
                        hm.put("proton", edProt1.text.toString())
                        hm.put("neutron", edNeu1.text.toString())
                        hm.put("nama_gol", pilihGolongan)
                        hm.put("keterangan", edKet1.text.toString())
                        hm.put("image", imStr)
                        hm.put("file", nmFile)
                    }
                    "delete" -> {
                        hm.put("mode", "delete")
                        hm.put("no_atom", edNoAt1.text.toString())
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaGolongan(){
        val request = object :StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarGolongan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarGolongan.add(jsonObject.getString("nama_gol"))
                }
                GolAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            })
        {
//            override fun getParams(): MutableMap<String, String>{
//                val hm = HashMap<String, String>()
//                hm.put("nama_prodi",namaProdi)
//                return hm
//            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataUnsur(namaUns : String){
        val request = object :StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarUnsur.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var uns = HashMap<String, String>()
                    uns.put("no_atom", jsonObject.getString("no_atom"))
                    uns.put("simbol", jsonObject.getString("simbol"))
                    uns.put("nama_unsur", jsonObject.getString("nama_unsur"))
                    uns.put("massa_atom", jsonObject.getString("massa_atom"))
                    uns.put("electron", jsonObject.getString("electron"))
                    uns.put("proton", jsonObject.getString("proton"))
                    uns.put("neutron", jsonObject.getString("neutron"))
                    uns.put("nama_gol", jsonObject.getString("nama_gol"))
                    uns.put("keterangan", jsonObject.getString("keterangan"))
                    uns.put("url", jsonObject.getString("url"))
                    daftarUnsur.add(uns)
                }
                UnsurAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_unsur",namaUns)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showDataUnsur("")
        getNamaGolongan()
    }
    fun clearInput(){
        val data = ""
        iduns = ""
        edNoAt1.setText("")
        edSim1.setText("")
        edNama1.setText("")
        edMassa1.setText("")
        edEle1.setText("")
        edProt1.setText("")
        edNeu1.setText("")
        edKet1.setText("")
        imageView2.setImageResource(android.R.drawable.ic_menu_camera)
    }
}