package santoso.eriawan.pembelajarankimia

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_about.*
import kotlinx.android.synthetic.main.activity_main.*

class AboutActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var preferences: SharedPreferences
    val PREF_NAME = "Setting"
    val RC_SUKSES: Int = 100


    var fontheader: String = ""
    val DEF_BACK_FONT_HEAD = fontheader
    val FIELD_FONTHEAD = "BACKGROUND_FONTHEADER"


    var fSubTitle: Int = 14
    val DEF_FONT_SUBTITLE = fSubTitle
    val FIELD_FONT_SUBTITLE = "Font_Size_SubTitle"


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_option, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onClick(v: View?) {
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)
        preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
        fSubTitle = preferences.getInt(FIELD_FONT_SUBTITLE, DEF_FONT_SUBTITLE)
        fontheader = preferences.getString(FIELD_FONTHEAD, DEF_BACK_FONT_HEAD).toString()

        fontheadercolor()
        subtitle()

        val actionBar = supportActionBar
        actionBar!!.title = "About Chemistry"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun fontheadercolor() {
        if (fontheader == "Blue") {
            txtHeader.setTextColor(Color.BLUE)
        } else if (fontheader == "Yellow") {
            txtHeader.setTextColor(Color.YELLOW)
        } else if (fontheader == "Green") {
            txtHeader.setTextColor(Color.GREEN)
        } else if (fontheader == "Black") {
            txtHeader.setTextColor(Color.BLACK)
        } else {
            txtHeader.setTextColor(Color.BLACK)
        }
    }

    fun subtitle() {
        txtSubtitle.textSize = fSubTitle.toFloat()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == RC_SUKSES)
                fontheader = data?.extras?.getString("fhColor").toString()
            fSubTitle = data?.extras?.getInt("tHead").toString().toInt()
            preferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
            val prefEdit = preferences.edit()
            prefEdit.putString(FIELD_FONTHEAD, fontheader)
            prefEdit.putInt(FIELD_FONT_SUBTITLE, fSubTitle)

            fontheadercolor()
            subtitle()
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item?.itemId) {
            R.id.itemSet -> {
                var intent = Intent(this, SettingActivity::class.java)
                startActivityForResult(intent, RC_SUKSES)
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

}

