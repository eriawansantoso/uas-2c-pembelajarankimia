package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_crud.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class FragmentCrud : Fragment(), View.OnClickListener {
    lateinit var thisParent: MainActivity
    lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_crud, container, false)
        v.btnUnsur.setOnClickListener(this)
        v.btnRumus.setOnClickListener(this)
        v.btnVideo.setOnClickListener(this)

        return v
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnVideo -> {
                val w = Intent(thisParent, ElektrolitActivity::class.java)
                startActivity(w)
            }
            R.id.btnRumus -> {
                val w = Intent(thisParent, SenyawaActivity::class.java)
                startActivity(w)
            }
            R.id.btnUnsur -> {
                val w = Intent(thisParent, UnsurActivity::class.java)
                startActivity(w)
            }

        }
    }
}