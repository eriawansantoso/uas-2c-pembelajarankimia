package santoso.eriawan.pembelajarankimia

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_elektrolit.*
import org.json.JSONArray
import org.json.JSONObject

class ElektrolitActivity: AppCompatActivity(), View.OnClickListener{

    lateinit var elAdapter : AdapterElektrolit
    var daftarEl = mutableListOf<HashMap<String,String>>()
    var id_elektrolit = ""
    val mainUrl = "http://192.168.43.104/uas-2c-kimiawebserver/"
    val url = mainUrl+"show_elektrolit.php"
    val url2 = mainUrl+"query_elektrolit.php"

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsEl -> {
                queryEl("insert")
            }
            R.id.btnUpEl -> {
                queryEl("update")
            }
            R.id.btnDelEl -> {
                queryEl("delete")
            }
            R.id.btnFindEl -> {
                showData(edNamaEl.text.toString())
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_elektrolit)
        elAdapter = AdapterElektrolit(daftarEl,this)
        lsEl.layoutManager = LinearLayoutManager(this)
        lsEl.adapter = elAdapter
        btnInsEl.setOnClickListener(this)
        btnUpEl.setOnClickListener(this)
        btnDelEl.setOnClickListener(this)
        btnFindEl.setOnClickListener(this)

        val actionBar = supportActionBar
        actionBar!!.title = "Kelola Data Elektrolit"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    fun showData(namaEl : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarEl.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sen = HashMap<String,String>()
                    sen.put("id_elektrolit",jsonObject.getString("id_elektrolit"))
                    sen.put("nama_larutan",jsonObject.getString("nama_larutan"))
                    sen.put("jenis_elektrolit",jsonObject.getString("jenis_elektrolit"))

                    daftarEl.add(sen)
                }
                elAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_larutan",namaEl)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun queryEl(mode : String){
        val request = object : StringRequest(
            Method.POST,url2,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if(error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil", Toast.LENGTH_LONG).show()
                    showData("")
                    clearInput()
                }else{
                    Toast.makeText(this,"Operasi Gagal", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "update" -> {
                        hm.put("mode","update")
                        hm.put("id_elektrolit",id_elektrolit)
                        hm.put("nama_larutan",edNamaEl.text.toString())
                        hm.put("jenis_elektrolit",edJenisEl.text.toString())

                    }
                    "insert" -> {
                        hm.put("mode","insert")

                        hm.put("nama_larutan",edNamaEl.text.toString())
                        hm.put("jenis_elektrolit",edJenisEl.text.toString())
                    }
                    "delete" -> {
                        hm.put("mode","delete")
                        hm.put("id_elektrolit",id_elektrolit)
                    }

                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    override fun onStart() {
        super.onStart()
        showData("")
    }

    fun clearInput(){
        id_elektrolit = ""
        edNamaEl.setText("")
        edJenisEl.setText("")

    }
}