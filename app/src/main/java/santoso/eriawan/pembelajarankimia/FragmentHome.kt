package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_home.view.*

class FragmentHome : Fragment(), View.OnClickListener {
    lateinit var thisParent: MainActivity
    lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_home, container, false)
        v.btnListUns.setOnClickListener(this)
        v.btnListSen.setOnClickListener(this)

        v.btnListEl.setOnClickListener(this)


        return v
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnListSen -> {
                val w = Intent(thisParent, SenyawaListActivity::class.java)
                startActivity(w)
            }
            R.id.btnListEl -> {
                val w = Intent(thisParent, ElektrolitListActivity::class.java)
                startActivity(w)
            }
            R.id.btnListUns -> {
                val w = Intent(thisParent, ListUnsurActivity::class.java)
                startActivity(w)
            }


        }
    }
}