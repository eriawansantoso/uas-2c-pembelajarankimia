package santoso.eriawan.pembelajarankimia

import android.app.Activity
import android.media.MediaPlayer
import android.net.Uri
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.MediaController
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_list_unsur.*
import kotlinx.android.synthetic.main.activity_unsur.*
import kotlinx.android.synthetic.main.activity_video.*
import org.json.JSONArray

class VideoActivity : AppCompatActivity(), View.OnClickListener{
    var posVideoSkrg = 0
    var posCoverSkr = 0
    var posJudulSkr = ""
    var handler = Handler()
    lateinit var mediaPlayer: MediaPlayer
    lateinit var mediaController: MediaController

    val daftarVideo = intArrayOf(R.raw.vid1,R.raw.vid2,R.raw.vid3)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
//

        mediaController = MediaController(this)
        mediaPlayer = MediaPlayer()
        mediaController.setPrevNextListeners(nextVid,prevVid)
        mediaController.setAnchorView(videoView)
        videoView.setMediaController(mediaController)
        videoSet(posVideoSkrg)

        val actionBar = supportActionBar
        actionBar!!.title = "Chemistry Video"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    var nextVid = View.OnClickListener { v: View? ->
        if (posVideoSkrg<(daftarVideo.size-1)) posVideoSkrg++
        else posVideoSkrg = 0
        videoSet(posVideoSkrg)
    }
    var prevVid = View.OnClickListener { v: View? ->
        if (posVideoSkrg>0) posVideoSkrg--
        else posVideoSkrg = daftarVideo.size-1
        videoSet(posVideoSkrg)
    }
    fun videoSet(pos: Int){
        videoView.setVideoURI(Uri.parse("android.resource://"+packageName+"/"+daftarVideo[pos]))

        mediaPlayer.stop()
        mediaPlayer.start()


    }

//
    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }



}