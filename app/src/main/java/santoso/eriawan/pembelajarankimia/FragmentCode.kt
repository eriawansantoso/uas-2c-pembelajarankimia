package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.zxing.BarcodeFormat
import com.journeyapps.barcodescanner.BarcodeEncoder
import kotlinx.android.synthetic.main.fragment_qrcode.view.*
import kotlinx.android.synthetic.main.fragment_qrcode.view.imQr

class FragmentCode : Fragment(), View.OnClickListener {
    lateinit var thisParent: MainActivity
    lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val barCodeEncorder = BarcodeEncoder()
        thisParent = activity as MainActivity
        val bitmap = barCodeEncorder.encodeBitmap("mailto:rizkadewi95@gmail.com",
            BarcodeFormat.QR_CODE, 400, 400)

        v = inflater.inflate(R.layout.fragment_qrcode, container, false)
        v.btnIG.setOnClickListener(this)
        v.imQr.setImageBitmap(bitmap)
        v.btnFB.setOnClickListener(this)


        return v
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnIG -> {
                var webUri = "https://www.instagram.com/eriawan_santoso/?hl=id"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)

            }
            R.id.btnFB -> {
                var webUri = "https://web.facebook.com/rizka.d.yuniar.73?ref=bookmarks"
                var intentInternet = Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse(webUri)
                ) //filter application than can open website
                startActivity(intentInternet)

            }

//

        }
    }
}