package santoso.eriawan.pembelajarankimia

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_setting.*

class SettingActivity : AppCompatActivity(), View.OnClickListener{

    var fontheader : String = ""
    var fSubTitle : Int = 0


    val sb1 = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            fSubTitle = progress.toFloat().toInt()
        }

        override fun onStartTrackingTouch(seekBar: SeekBar?) {

        }

        override fun onStopTrackingTouch(seekBar: SeekBar?) {

        }

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_setting)
        sbSubtitle.setOnSeekBarChangeListener(sb1)
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rbBlue -> fontheader = "Blue"
                R.id.rbYellow -> fontheader = "Yellow"
                R.id.rbGreen -> fontheader = "Green"
                R.id.rbBlack -> fontheader = "Black"
            }
            btSimpan.setOnClickListener(this)
        }
        val actionBar = supportActionBar
        actionBar!!.title = "Setting"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }


    override fun onClick(v: View?) {
        Toast.makeText(this, "Berhasil Menyimpan", Toast.LENGTH_SHORT).show()
        finish()
    }

    override fun finish() {
        var intent = Intent()
        intent.putExtra("fhColor",fontheader)
        intent.putExtra("tHead",fSubTitle)
        setResult(Activity.RESULT_OK,intent)
        super.finish()
    }


}