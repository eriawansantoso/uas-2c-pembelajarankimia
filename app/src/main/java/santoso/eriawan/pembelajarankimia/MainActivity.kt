package santoso.eriawan.pembelajarankimia

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() , BottomNavigationView.OnNavigationItemSelectedListener{

    lateinit var fragHome : FragmentHome
    lateinit var fragCrud : FragmentCrud
    lateinit var fragQR : FragmentCode
    lateinit var fragmentAbout: FragmentAbout
    lateinit var ft : FragmentTransaction

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragHome = FragmentHome()
        fragCrud = FragmentCrud()
        fragQR = FragmentCode()
        fragmentAbout = FragmentAbout()

        val actionBar = supportActionBar
        actionBar!!.title = "CHemistry"

        ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.frameLayout, fragHome).commit()
        frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
        frameLayout.visibility = View.VISIBLE

    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {

            R.id.itemHome -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragHome).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemCrud -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragCrud).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }

            R.id.itemBarcode -> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragQR).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemAbout-> {
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout, fragmentAbout).commit()
                frameLayout.setBackgroundColor(Color.argb(245, 255, 255, 255))
                frameLayout.visibility = View.VISIBLE
            }

        }
        return true
    }
}
