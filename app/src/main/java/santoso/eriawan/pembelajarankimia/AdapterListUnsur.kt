package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_list_unsur.*

class AdapterListUnsur (val dataUnsur : List<HashMap<String, String>>,
                    val list : ListUnsurActivity) : //new
                    RecyclerView.Adapter<AdapterListUnsur.HolderLUnsur>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterListUnsur.HolderLUnsur {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_unsur,p0,false)
        return HolderLUnsur(v)
    }
    override fun getItemCount(): Int {
        return dataUnsur.size
    }
    override fun onBindViewHolder(p0: AdapterListUnsur.HolderLUnsur, p1: Int) {
        val data = dataUnsur.get(p1)
        p0.txNomor.setText(data.get("no_atom"))
        p0.txSimbol.setText(data.get("simbol"))
        p0.txNama.setText(data.get("nama_unsur"))
        p0.txMassa.setText(data.get("massa_atom"))
        p0.txElek.setText(data.get("electron"))
        p0.txProton.setText(data.get("proton"))
        p0.txNeu.setText(data.get("neutron"))
        p0.txGolongan.setText(data.get("nama_gol"))
        p0.txKet.setText(data.get("keterangan"))
        //begin new
        if (p1.rem(2) == 0) p0.cLayout.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.cLayout.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayout.setOnClickListener({
            list.edCariUnsur.setText("nama_unsur")

        })

        if (!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(p0.photo);
    }
    inner class HolderLUnsur(v : View) : RecyclerView.ViewHolder(v) {
        val txNomor = v.findViewById<TextView>(R.id.txNomor)
        val txSimbol = v.findViewById<TextView>(R.id.txSimbol)
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txMassa = v.findViewById<TextView>(R.id.txMassa)
        val txElek = v.findViewById<TextView>(R.id.txElek)
        val txProton = v.findViewById<TextView>(R.id.txProton)
        val txNeu = v.findViewById<TextView>(R.id.txNeu)
        val txGolongan = v.findViewById<TextView>(R.id.txGolongan)
        val txKet = v.findViewById<TextView>(R.id.txKet)
        val photo = v.findViewById<ImageView>(R.id.img)
        val cLayout = v.findViewById<ConstraintLayout>(R.id.cLayout) //new
    }
}