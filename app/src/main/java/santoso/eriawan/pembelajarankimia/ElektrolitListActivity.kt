package santoso.eriawan.pembelajarankimia

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_elektrolit_list.*
import org.json.JSONArray

class ElektrolitListActivity  : AppCompatActivity(), View.OnClickListener{

    lateinit var elektrolitAdapter: AdapterElektrolitList
    var daftarEl = mutableListOf<HashMap<String, String>>()
    val mainurl = "http://192.168.43.104/uas-2c-kimiawebserver/"
    val url = mainurl+"show_elektrolit.php"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_elektrolit_list)
        elektrolitAdapter = AdapterElektrolitList(daftarEl, this)
        lsListEl.layoutManager = LinearLayoutManager(this)
        lsListEl.adapter =elektrolitAdapter

        btnFindEl.setOnClickListener(this)

        val actionBar = supportActionBar
        actionBar!!.title = "Larutan Elektrolit"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnFindEl -> {
                show(edCariElek.text.toString().trim())
            }
        }
    }
    fun show(namaEl : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarEl.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var sen = HashMap<String,String>()
                    sen.put("id_elektrolit",jsonObject.getString("id_elektrolit"))
                    sen.put("nama_larutan",jsonObject.getString("nama_larutan"))
                    sen.put("jenis_elektrolit",jsonObject.getString("jenis_elektrolit"))
                    daftarEl.add(sen)
                }
                elektrolitAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_larutan",namaEl)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
    override fun onStart() {
        super.onStart()
        show("")
    }
}