package santoso.eriawan.pembelajarankimia

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_senyawa.*
import kotlinx.android.synthetic.main.activity_senyawa.view.*
import kotlinx.android.synthetic.main.activity_senyawa.view.edAnion
import kotlinx.android.synthetic.main.activity_senyawa.view.edKation
import kotlinx.android.synthetic.main.activity_senyawa.view.edNmSenyawa
import kotlinx.android.synthetic.main.activity_senyawa.view.edRumus
import kotlinx.android.synthetic.main.row_senyawa.view.*

class AdapterSenyawa (val dataSenyawa : List<HashMap<String, String>>,
                      val mainSenyawa : SenyawaActivity) : //new
    RecyclerView.Adapter<AdapterSenyawa.HolderSenyawa>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterSenyawa.HolderSenyawa {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_senyawa,p0,false)
        return HolderSenyawa(v)
    }
    override fun getItemCount(): Int {
        return dataSenyawa.size
    }
    override fun onBindViewHolder(holder: AdapterSenyawa.HolderSenyawa, position: Int) {
        val data = dataSenyawa.get(position)
        holder.txKation.setText(data.get("kation"))
        holder.txAnion.setText(data.get("anion"))
        holder.txRumus.setText(data.get("rumus_senyawa"))
        holder.txNamaSe.setText(data.get("nama_senyawa"))
        if(position.rem(2) == 0) holder.cLayoutSe.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayoutSe.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayoutSe.setOnClickListener({
            mainSenyawa.id_senyawa = data.get("id_senyawa").toString()
            mainSenyawa.edKation.setText(data.get("kation"))
            mainSenyawa.edAnion.setText(data.get("anion"))
            mainSenyawa.edRumus.setText(data.get("rumus_senyawa"))
            mainSenyawa.edNmSenyawa.setText(data.get("nama_senyawa"))
        })
    }

    class HolderSenyawa(v: View) : RecyclerView.ViewHolder(v) {
        val txKation = v.findViewById<TextView>(R.id.txKation)
        val txAnion = v.findViewById<TextView>(R.id.txAnion)
        val txRumus = v.findViewById<TextView>(R.id.txRumus)
        val txNamaSe = v.findViewById<TextView>(R.id.txNamaSe)
        val cLayoutSe = v.findViewById<ConstraintLayout>(R.id.cLayoutSe)
    }
}