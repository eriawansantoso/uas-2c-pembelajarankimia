package santoso.eriawan.pembelajarankimia

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_elektrolit.*
import kotlinx.android.synthetic.main.activity_senyawa.*

class AdapterElektrolit  (val dataEl : List<HashMap<String, String>>,
                          val mainEl : ElektrolitActivity) : //new
    RecyclerView.Adapter<AdapterElektrolit.HolderEl>() {

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterElektrolit.HolderEl {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_elektrolit,p0,false)
        return HolderEl(v)
    }
    override fun getItemCount(): Int {
        return dataEl.size
    }
    override fun onBindViewHolder(holder: AdapterElektrolit.HolderEl , position: Int) {
        val data = dataEl.get(position)
        holder.txNamaEl.setText(data.get("nama_larutan"))
        holder.txJenis.setText(data.get("jenis_elektrolit"))

        if(position.rem(2) == 0) holder.cLayoutEl.setBackgroundColor(
            Color.rgb(230,245,240))
        else holder.cLayoutEl.setBackgroundColor(Color.rgb(255,255,245))

        holder.cLayoutEl.setOnClickListener({
            mainEl.id_elektrolit = data.get("id_elektrolit").toString()
            mainEl.edNamaEl.setText(data.get("nama_larutan"))
            mainEl.edJenisEl.setText(data.get("jenis_elektrolit"))

        })
    }

    class HolderEl(v: View) : RecyclerView.ViewHolder(v) {
        val txNamaEl= v.findViewById<TextView>(R.id.txNamaEl)
        val txJenis = v.findViewById<TextView>(R.id.txJenis)

        val cLayoutEl = v.findViewById<ConstraintLayout>(R.id.cLayoutEl)
    }
}