package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_senyawa_list.*

class AdapterSenyawaList (val dataUnsur : List<HashMap<String, String>>,
                          val list : SenyawaListActivity) : //new
                    RecyclerView.Adapter<AdapterSenyawaList.HolderLUnsur>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterSenyawaList.HolderLUnsur {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_senyawa,p0,false)
        return HolderLUnsur(v)
    }
    override fun getItemCount(): Int {
        return dataUnsur.size
    }
    override fun onBindViewHolder(p0: AdapterSenyawaList.HolderLUnsur, p1: Int) {
        val data = dataUnsur.get(p1)
        p0.txKation.setText(data.get("kation"))
        p0.txAnion.setText(data.get("anion"))
        p0.txRumus.setText(data.get("rumus_senyawa"))
        p0.txNamaSe.setText(data.get("nama_senyawa"))
        //begin new
        if (p1.rem(2) == 0) p0.cLayoutSe.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.cLayoutSe.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayoutSe.setOnClickListener({
            list.edCariSen.setText("nama_unsur")

//            Intent e = new Intent(context , VideoActivity::class.java)
//            context.startActivity(e)


        })


    }
    inner class HolderLUnsur(v : View) : RecyclerView.ViewHolder(v) {
        val txKation = v.findViewById<TextView>(R.id.txKation)
        val txAnion = v.findViewById<TextView>(R.id.txAnion)
        val txRumus = v.findViewById<TextView>(R.id.txRumus)
        val txNamaSe = v.findViewById<TextView>(R.id.txNamaSe)
        val cLayoutSe = v.findViewById<ConstraintLayout>(R.id.cLayoutSe)
    }
}