package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_about.view.*
import kotlinx.android.synthetic.main.fragment_home.view.*

class FragmentAbout : Fragment(), View.OnClickListener {
    lateinit var thisParent: MainActivity
    lateinit var v: View

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        thisParent = activity as MainActivity
        v = inflater.inflate(R.layout.fragment_about, container, false)
        v.btnAbout.setOnClickListener(this)
        v.btnVid.setOnClickListener(this)

        return v
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnAbout -> {
                val show = Intent(thisParent, AboutActivity::class.java)
                startActivity(show)
            }
            R.id.btnVid -> {
                val s = Intent(thisParent, VideoActivity::class.java)
                startActivity(s)
            }
        }
    }
}