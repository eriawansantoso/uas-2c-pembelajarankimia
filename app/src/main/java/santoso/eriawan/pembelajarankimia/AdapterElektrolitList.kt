package santoso.eriawan.pembelajarankimia

import android.content.Intent
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_elektrolit_list.*
import kotlinx.android.synthetic.main.activity_senyawa_list.*

class AdapterElektrolitList (val dataElek : List<HashMap<String, String>>,
                             val list : ElektrolitListActivity) : //new
                    RecyclerView.Adapter<AdapterElektrolitList.HolderLElek>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): AdapterElektrolitList.HolderLElek {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.row_elektrolit,p0,false)
        return HolderLElek(v)
    }
    override fun getItemCount(): Int {
        return dataElek.size
    }
    override fun onBindViewHolder(p0: AdapterElektrolitList.HolderLElek, p1: Int) {
        val data = dataElek.get(p1)
        p0.txNamaEl.setText(data.get("nama_larutan"))
        p0.txJenis.setText(data.get("jenis_elektrolit"))
        //begin new
        if (p1.rem(2) == 0) p0.cLayoutEl.setBackgroundColor(
            Color.rgb(230, 245, 240))
        else p0.cLayoutEl.setBackgroundColor(Color.rgb(255, 255, 245))

        p0.cLayoutEl.setOnClickListener({
            list.edCariElek.setText("nama_larutan")


        })


    }
    inner class HolderLElek(v : View) : RecyclerView.ViewHolder(v) {
        val txNamaEl = v.findViewById<TextView>(R.id.txNamaEl)
        val txJenis = v.findViewById<TextView>(R.id.txJenis)
        val cLayoutEl = v.findViewById<ConstraintLayout>(R.id.cLayoutEl)
    }
}