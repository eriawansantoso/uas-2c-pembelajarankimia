package santoso.eriawan.pembelajarankimia

import android.os.Bundle
import android.os.PersistableBundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.activity_list_unsur.*
import kotlinx.android.synthetic.main.activity_unsur.*
import org.json.JSONArray

class ListUnsurActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var mediaHelper: MediaHelper
    lateinit var UnsurAdapter: AdapterListUnsur
    lateinit var GolAdapter: ArrayAdapter<String>
    var daftarUnsur = mutableListOf<HashMap<String, String>>()
    var daftarGolongan = mutableListOf<String>()
    val mainurl = "http://192.168.43.104/andro_uas/"
    val url = mainurl+"show_unsur.php"
    val url2 = mainurl+"show_golongan.php"
    var imStr = ""
    var pilihGolongan = ""
    var iduns = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_unsur)
        UnsurAdapter = AdapterListUnsur(daftarUnsur, this)
        mediaHelper = MediaHelper(this)
        lsListUnsur.layoutManager = LinearLayoutManager(this)
        lsListUnsur.adapter = UnsurAdapter

        btnCariUns.setOnClickListener(this)
        val actionBar = supportActionBar
        actionBar!!.title = "Unsur Kimia"
        actionBar.setDisplayHomeAsUpEnabled(true)
        actionBar.setDisplayHomeAsUpEnabled(true)
    }
    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnCariUns -> {
                showData(edCariUnsur.text.toString().trim())
            }
        }
    }

    fun showData(namaUns : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarUnsur.clear()
                val jsonArray = JSONArray(response)
                for(x in 0 .. (jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var uns = HashMap<String, String>()
                    uns.put("no_atom", jsonObject.getString("no_atom"))
                    uns.put("simbol", jsonObject.getString("simbol"))
                    uns.put("nama_unsur", jsonObject.getString("nama_unsur"))
                    uns.put("massa_atom", jsonObject.getString("massa_atom"))
                    uns.put("electron", jsonObject.getString("electron"))
                    uns.put("proton", jsonObject.getString("proton"))
                    uns.put("neutron", jsonObject.getString("neutron"))
                    uns.put("nama_gol", jsonObject.getString("nama_gol"))
                    uns.put("keterangan", jsonObject.getString("keterangan"))
                    uns.put("url", jsonObject.getString("url"))
                    daftarUnsur.add(uns)
                }
                UnsurAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_LONG).show()
            }){
            override fun getParams(): MutableMap<String, String>{
                val hm = HashMap<String, String>()
                hm.put("nama_unsur",namaUns)
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onStart() {
        super.onStart()
        showData("")
    }
}